/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

/**
 * This class has different methods to calculate different things
 * of a given DNA sequence that has been read from a file .txt
 * @author tarda
 */
public class ADN_Manager {

    /**
     * Funcio que agafa l'atribut ADN i el retorna invertit
     *
     * @param ADN
     * @return ADN invertit.
     */
    public String invertADN(String ADN) {
        StringBuilder builder = new StringBuilder(ADN);
        return builder.reverse().toString();
    }

    /**
     * Fa recompte de totes les A's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numAdenines(String ADN) {
        int a = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'A') {
                a++;
            }
        }
        return a;
    }

    /**
     * This function calculates the percent of adenines
     *
     * @param ADN
     * @return percent of adenines
     */
    public double percentAdenines(String ADN) {
        double lengthADN = ADN.length();
        double numAdenines = numAdenines(ADN);
        double percentAdenines = numAdenines / lengthADN * 100;
        return percentAdenines;
    }

    /**
     * Fa recompte de totes les G's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numGuanines(String ADN) {
        int g = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'G') {
                g++;
            }
        }
        return g;
    }

    /**
     * This function calculates the percent of guanines
     *
     * @param ADN
     * @return percent of guanines
     */
    public double percentGuanines(String ADN) {
        double length = ADN.length();
        double numGuanines = numGuanines(ADN);
        double percentGuanines = numGuanines / length * 100;
        return percentGuanines;
    }

    /**
     * Fa recompte de totes les T's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numTimines(String ADN) {
        int t = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'T') {
                t++;
            }
        }
        return t;
    }

    /**
     * This function calculates the percent of timines
     *
     * @param ADN
     * @return percent of timines
     */
    public double percentTimines(String ADN) {
        double length = ADN.length();
        double numTimines = numTimines(ADN);
        double percentTimines = numTimines / length * 100;
        return percentTimines;
    }

    /**
     * Fa recompte de totes les C's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numCitosines(String ADN) {
        int c = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'C') {
                c++;
            }
        }
        return c;
    }

    /**
     * This function calculates the percent of citosines
     *
     * @param ADN
     * @return percent of citosines
     */
    public double percentCitosines(String ADN) {
        double length = ADN.length();
        double numCitosines = numCitosines(ADN);
        double percentCitosines = numCitosines / length * 100;
        return percentCitosines;
    }

    /**
     * Funcio que compara el recompte de totes les lletres
     *
     * @param ADN
     * @return La lletra que té mes recompte que la resta
     */
    public String maxLetter(String ADN) {
        int max = 0;
        String base;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);

        if (a > c && a > g && a > t) {
            base = "A";
            max = a;
        } else if (c > a && c > g && c > t) {
            base = "C";
            max = c;
        } else if (g > a && g > c && g > t) {
            base = "G";
            max = g;
        } else {
            base = "T";
            max = t;
        }

        return base;
    }

    /**
     * Funcio que compara el recompte de totes les lletres
     *
     * @param ADN
     * @return La lletra que té menys recompte que la resta
     *
     */
    public String minLetter(String ADN) {
        int min = 0;
        String base;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);

        if (a < c && a < g && a < t) {
            base = "A";
            min = a;
        } else if (c < a && c < g && c < t) {
            base = "C";
            min = c;
        } else if (g < a && g < c && g < t) {
            base = "G";
            min = g;
        } else {
            base = "T";
            min = t;
        }

        return base;
    }
}
