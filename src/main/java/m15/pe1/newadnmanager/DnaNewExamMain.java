package m15.pe1.newadnmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class DnaNewExamMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        DnaNewExamMain myApp = new DnaNewExamMain();
        myApp.run();
    }

    /**
     * Function that runs the app
     */
    private void run() {
        ADN_Manager cadenaADN = new ADN_Manager();
        newADNFileReader file = new newADNFileReader();
        ArrayList<String> dnaSequence_list = file.readSequence("src/main/java/m15/pe1/newadnmanager/dnaSequence.txt");
        String dnaSequence = String.join("", dnaSequence_list);
        dnaSequence = dnaSequence.toUpperCase();

        menu(dnaSequence, cadenaADN);
    }

    private static void menu(String dnaSequence, ADN_Manager cadenaADN) {
        int option;
        do {
            System.out.println("\n Tria una opció:");
            System.out.println("0.-Sortir");
            System.out.println("1.-Donar la volta\n"
                    + "2.- Trobar la base més repetida,\n"
                    + "3.- Trobar la base menys repetida\n"
                    + "4.- Fer recompte de bases\n");
            System.out.print("\nOpció: ");
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();

            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    System.out.println("**Donar la volta " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(cadenaADN.invertADN(dnaSequence));
                    break;
                case 2:
                    System.out.println("**Trobar la base més repetida " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(cadenaADN.maxLetter(dnaSequence));
                    break;
                case 3:
                    System.out.println("**Trobar la base menys repetida " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(cadenaADN.minLetter(dnaSequence));
                    break;
                case 4:
                    System.out.println("**Fer recompte de bases " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(TOTAL_DE_ + "A: " + cadenaADN.numAdenines(dnaSequence));
                    System.out.println(TOTAL_DE_ + "C: " + cadenaADN.numCitosines(dnaSequence));
                    System.out.println(TOTAL_DE_ + "G: " + cadenaADN.numGuanines(dnaSequence));
                    System.out.println(TOTAL_DE_ + "T: " + cadenaADN.numTimines(dnaSequence));
                    System.out.println(PERCENT_OF_ + "A: " + cadenaADN.percentAdenines(dnaSequence) + "%");
                    System.out.println(PERCENT_OF_ + "C: " + cadenaADN.percentCitosines(dnaSequence) + "%");
                    System.out.println(PERCENT_OF_ + "G: " + cadenaADN.percentGuanines(dnaSequence) + "%");
                    System.out.println(PERCENT_OF_ + "T: " + cadenaADN.percentTimines(dnaSequence) + "%");
                    break;
                default:
                    System.out.println("La opció seleccionada no és válida.");
                    break;
            }
        } while (option != 0);
    }
    private static final String PERCENT_OF_ = "Percent of ";
    private static final String TOTAL_DE_ = "Total de ";

    private void ShowSecuences(String dnaSequence, String dnaSubSequence) {
        System.out.println("DNA sequence: " + dnaSequence);
        System.out.println("DNA subSequence: " + dnaSubSequence);
    }
}
